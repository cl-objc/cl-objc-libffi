(in-package "CL-OBJC-EXAMPLES")

(define-objc-class my-document ns-document
  ((text-view ns-text-view)
   (m-string ns-attributed-string)))

(define-objc-method init () ((self my-document))
  (with-super
    (objc:init self))
  self)

(define-objc-method string () ((self my-document))
  (with-ivar-accessors my-document
    (objc:autorelease (objc:retain (m-string self)))))

(define-objc-method :set-string (:return-type :void) ((self my-document) (new-value ns-attributed-string))
  (with-ivar-accessors my-document
    (unless (eq (m-string self) new-value)
      (setf (m-string self) (objc:copy new-value)))))

(define-objc-method text-did-change (:return-type :void) ((self my-document) (notification ns-notification))
  (declare (ignore notification))
  (with-ivar-accessors my-document
    (objc:set-string? self (objc:text-storage (text-view self)))))

(define-objc-method (:data-of-type :error) () ((self my-document) (error))
  (declare (ignore error))
  (with-ivar-accessors my-document
    (objc:set-string? self (text-view self))
    (objc:archived-data-with-root-object? (meta 'objc:ns-archiver) (string self))))

(define-objc-method (:read-from-data :of-type :error) (:return-type :boolean) ((self my-document) (data ns-data) (type) (error))
  (declare (ignore error type))
  (let ((temp-string (objc:unarchive-object-with-data? (meta 'objc:ns-unarchiver) data)))
    (objc:set-string? self temp-string)))

(define-objc-method window-nib-name () ((self my-document))
  (objc-let ((ret 'ns-string :init-with-utf8-string "MyDocument"))
    ret))

(define-objc-method :window-controller-did-load-nib (:return-type :void) ((self my-document) (controller ns-window-controller))
  (with-super
    (objc:window-controller-did-load-nib? self controller))
  (with-ivar-accessors my-document
    (unless (objc-nil-object-p (m-string self))
      (objc:set-attributed-string? (objc:text-storage (text-view self)) (objc:string self)))))

(define-objc-method :data-representation-of-type () ((self my-document) (type ns-string))
  (declare (ignore type))
  objc-nil-object)

(define-objc-method (:load-data-representation :of-type) (:return-type :boolean) ((self my-document) (type ns-string))
  (declare (ignore type))
  t)

(defun text-editor ()
  (swank:create-server :port 5555 :dont-close t)
  (invoke 'ns-application shared-application)
  (invoke 'ns-bundle :load-nib-named (invoke (invoke 'ns-string alloc) :init-with-utf8-string "MainMenu") :owner cl-objc::*nsapp*)
  (invoke cl-objc::*nsapp* run))