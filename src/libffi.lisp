(in-package "CL-USER")

(defpackage "LIBFFI"
  (:use "COMMON-LISP" "CFFI")
  (:export "FFI-PREP-CIF"
	   "FFI-CALL"
	   "FFI-CIF"
	   "CREATE-AGGREGATE-FFI-TYPE"
	   "FFI-TYPE-POINTER"
	   "FFI-TYPE-VOID"  
	   "FFI-TYPE-UINT8" 
	   "FFI-TYPE-SINT8" 
	   "FFI-TYPE-UINT16"
	   "FFI-TYPE-SINT16"
	   "FFI-TYPE-UINT32"
	   "FFI-TYPE-SINT32"
	   "FFI-TYPE-UINT64"
	   "FFI-TYPE-SINT64"
	   "FFI-TYPE-FLOAT" 
	   "FFI-TYPE-DOUBLE"
	   "FFI-TYPE-LONGDOUBLE"))

(in-package "LIBFFI")

(pushnew 
 (make-pathname :directory (append 
			    (butlast (pathname-directory *load-pathname*))
			    (list "libffi" ".libs"))) 
 *foreign-library-directories*
 :test #'equal)

(define-foreign-library libffi 
  (t (:default "libffi")))

(use-foreign-library libffi)

(defcstruct ffi-type
  (size :unsigned-long)
  (alignment :unsigned-short)
  (type :unsigned-short)
  (elements :pointer))

(defcvar ffi-type-void   ffi-type)
(defcvar ffi-type-uint8  ffi-type)
(defcvar ffi-type-sint8  ffi-type)
(defcvar ffi-type-uint16 ffi-type)
(defcvar ffi-type-sint16 ffi-type)
(defcvar ffi-type-uint32 ffi-type)
(defcvar ffi-type-sint32 ffi-type)
(defcvar ffi-type-uint64 ffi-type)
(defcvar ffi-type-sint64 ffi-type)
(defcvar ffi-type-float  ffi-type)
(defcvar ffi-type-double ffi-type)
(defcvar (ffi-type-pointer "ffi_type_pointer") ffi-type)

;; if have long doubles
(defcvar ffi-type-longdouble ffi-type)

(defcenum ffi-status
  :ffi-ok
  :ffi-bad-typedef
  :ffi-bad-abi)

(defcenum ffi-abi
  :ffi-first-abi
  :ffi-sysv
  :ffi-unix64
  (:ffi-default-abi 
   #+x86 1 ; sysv
   #+ppc 2 ; unix64
   ))

(defcstruct ffi-cif
  (abi ffi-abi)
  (nargs :unsigned-int)
  (arg-types :pointer)
  (rtype ffi-type)
  (bytes :unsigned-int)
  (flags :unsigned-int))

;;; Public interface
(defcfun ffi-prep-cif ffi-status
  (cif ffi-cif)
  (abi ffi-abi)
  (nargs :unsigned-int)
  (rtype ffi-type)
  (atypes :pointer))

(defcfun ffi-call :void
  (cif ffi-cif)
  (fn :pointer)
  (rvalue :pointer)
  (avalues :pointer))

(defconstant +ffi-type-struct+ 13)

(defun create-aggregate-ffi-type (field-types)
  (declare (optimize (debug 3) (space 0) (speed 0)))
  (let ((ret (foreign-alloc 'ffi-type))
	(elements (foreign-alloc 'ffi-type :count (1+ (length field-types)) :null-terminated-p t)))
    (setf (foreign-slot-value ret 'ffi-type 'size) 0
	  (foreign-slot-value ret 'ffi-type 'alignment) 0
	  (foreign-slot-value ret 'ffi-type 'elements) elements
	  (foreign-slot-value ret 'ffi-type 'type) +ffi-type-struct+)
    (loop
       for i upfrom 0
       for field-type = (pop field-types)
       while field-type
       do 
	 (if (eq (foreign-slot-value field-type 'ffi-type 'type) +ffi-type-struct+)
	     (loop
		for i upfrom 0
		for field = (mem-aref 
			     (foreign-slot-value field-type 'ffi-type 'elements) :pointer i)
		until (null-pointer-p field)
		do (push field field-types))
	     (setf (mem-aref elements 'ffi-type i) field-type)))
    (setf (mem-aref elements 'ffi-type (length field-types)) (null-pointer))
    ret))

(defun len-of-aggregate-type (type)
  (loop 
     for i upfrom 0
     for field = (mem-aref (foreign-slot-value type 'ffi-type 'elements) :pointer i)
     until (null-pointer-p field)
     finally (return (1+ i))))

#|
(in-package :libffi)

(use-package :cl-user)

(deflex cif (foreign-alloc 'ffi-cif))

(deflex atypes (foreign-alloc :pointer :count 1))
(setf (mem-ref atypes :pointer) ffi-type-sint64)

(ffi-prep-cif cif :ffi-sysv 1 ffi-type-sint64 atypes)

(deflex rvalue (foreign-alloc :int))

(deflex avalues (foreign-alloc :pointer :count 1))
(deflex avalue1 (foreign-alloc :int :initial-element -2))
(setf (mem-ref avalues :pointer) avalue1)

(ffi-call cif (foreign-symbol-pointer "abs") rvalue avalues)

(mem-ref rvalue :int)
|#

